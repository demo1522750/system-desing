## Container diagram

```plantuml 
@startuml

!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/actors
!define frontends https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/frontends
!define services_monobusiness https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/services/monobusiness
!define services_mono https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/services/mono
!define services_ext https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/services/ext


skinparam wrapWidth 200
' skinparam maxMessageSize 200


'LAYOUT_WITH_LEGEND()


!include actors/director.puml
!include actors/accountant.puml
!include frontends/mobile/mobile_app.puml

!include frontends/web/web_app.puml

  Rel_D(director, mobile_app, "Створює платежі, дивиться виписку")
  Rel_D(director, web_app, "Створює платежі, дивиться виписку")

System_Boundary(monobusiness-payment, "Payments") {


  
  !include frontends/bff/dymna-sumish/bff.puml
  '!include frontends/web/web_bff.puml
  
  Rel_D(mobile_app, mobile_and_web_bff, "Маршрутизация  трафика", "REST API")
  Rel_D(web_app, mobile_and_web_bff, "Маршрутизация  трафика", "REST API")
  
  !include services_monobusiness/payments-backend/normal.puml
  !include services_monobusiness/payments-backend/db.puml
  !include services_monobusiness/payments-backend/queue.puml
  
  Rel_D(mobile_and_web_bff, payments_backend, "Маршрутизация  трафика", "REST API")
  'Rel_D(web_bff, payments_backend, "Маршрутизация  трафика", "REST API")
  
  !include services_monobusiness/dragon-le/normal.puml
  !include services_monobusiness/dragon-le/queue.puml
  Rel( payments_backend_q, dragon_le_q, "backstream - зворотній поток від банку", "rabbitmq_shovel plugin")
  Rel( payments_backend_q, dragon_le_q, "drg_paym_result - отримання результату обробки проводок", "rabbitmq_shovel plugin")
  Rel( payments_backend, dragon_le,  "Створення платежів", "REST API")
  
  !include services_monobusiness/infoservice-le/normal.puml
  Rel(infoservice_le, payments_backend, "Праця з холдами та балансом", "REST API")
  
  !include services_monobusiness/jb/normal.puml
  !include services_monobusiness/jb/queue.puml
  Rel( jb_q, dragon_le_q, "Переніс повідомлень між чергами", "rabbitmq_shovel plugin")
  Rel( jb, payments_backend, "Довідник МФО", "REST API")
  
  !include services_monobusiness/payments-account-search/normal.puml
   Rel( payments_backend, payments_account_search, "Пошук рахунків", "REST API")
    
    !include services_monobusiness/payments-templates/normal.puml
    BiRel( payments_backend, payments_templates, "Шаблони платежів", "REST API")
    
    !include services_monobusiness/the-hardkiss/normal.puml
    Rel( the_hardkiss, payments_backend,  "Ролі, зв'язки", "REST API")
    
    !include services_monobusiness/statement-legal/normal.puml
    Rel( statement_legal, payments_backend,  "Виписка", "REST API")



}

System_Boundary(ext, "External systems") {
     !include services_ext/scrooge/ext.puml
    Rel_L( jb, scrooge_ext, "Данні по проводкам", "JDBC + GG")
}


System_Boundary(mono, "mono systems") {
    !include services_mono/auth/ext.puml
    !include services_mono/card_products_and_registration/nirvana/ext.puml
    !include services_mono/interactive/pdfbox/ext.puml
    !include services_mono/interactive/push_sender/ext.puml
    !include services_mono/interactive/trs/ext.puml
    !include services_mono/risks/guns-n-roses/ext.puml
    !include services_mono/risks/limits/ext.puml
    
    Rel( payments_backend, auth_ext, "Аутентификация", "REST API")
    Rel( payments_backend, nirvana_ext, "Данні по клієнту", "REST API")
    Rel( payments_backend, pdfbox_ext, "Квітанції, паперові звіти", "REST API")
    Rel( payments_backend, push_sender_ext, "Відправка повідомлень", "REST API")
    Rel( payments_backend, trs_ext, "Правила збогачення оперцій", "REST API")
    Rel( payments_backend, guns_n_roses_ext, "Правила .....", "REST API")
    Rel_U( payments_backend, limits_ext, "Довідник едрпоу", "REST API")
}




SHOW_LEGEND()

@enduml

```
## Use case

```plantuml

left to right direction
skinparam packageStyle rectangle

actor Бухгалтер as accountant
actor Діректор as director

rectangle "Платіжний сервіс" {
  usecase (UC-1 Створити платіж по IBAN) as UC1
  usecase (UC-2 Відмінити платіж) as UC2
  usecase (UC-3 Подивиться квитанцію) as UC3
  usecase (UC-4 Підписати платіж) as UC4
  
  url of UC1 is [[use-cases/uc-1-create-payment.md]]
  url of UC2 is [[use-cases/uc-2-cansel-payment.md]]
  url of UC3 is [[use-cases/uc-3-view-order.md]]
  url of UC4 is [[use-cases/uc-4-sign-payment.md]]
  
}

director --> UC1
UC1 <-- accountant

director --> UC2
UC2 <-- accountant

director --> UC3
UC3 <-- accountant

director --> UC4

```



**Use cases**
- [UC-1](use-cases/uc-1-create-payment.md) Створити платіж по IBAN.
