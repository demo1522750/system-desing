# UC-1 Створити платіж по IBAN

```plantuml
actor Директор as director
participant "mobile_app" as mobile_app
participant "le_mobile_bff" as mobile_bff
participant "le_payment" as le_payment
participant "le_dragon" as le_dragon
participant "jb" as jb
participant "bank" as bank

director -> mobile_app: Створити платіж по IBAN
mobile_app -> mobile_bff: Створити платіж по IBAN
note over mobile_bff: [POST] /api/v1/payments
mobile_bff -> le_payment: Проксирует запрос
le_payment -> le_payment: Створити платіж
le_payment --> mobile_bff: 200 OK
mobile_bff --> mobile_app: 200 OK
mobile_app --> director: Платіж створено
note over mobile_app: Экран "Платіж створено"
note over le_payment: [POST] /api/v1/payments
le_payment -> le_dragon: Cтворити платіж по IBAN
note over le_dragon: [POST] /api/v1/payments
le_dragon -> jb: Cтворити платіж по IBAN
jb -> bank: Cтворити платіж по IBAN
note over bank: [таблиця Х]
jb <-- bank: 200 OK
le_dragon <-- jb: 200 OK
le_payment <-- le_dragon: 200 OK

```
