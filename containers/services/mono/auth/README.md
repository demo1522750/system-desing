```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
'!define actors https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/actors
'!define frontends https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/frontends
!define services_mono https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/services/mono


!include services_mono/auth/ext.puml

!include services_mono/auth/normal.puml

!include services_mono/auth/db.puml

```