nmmhvhv

```plantuml

!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
!define services_monobusiness https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/services/monobusiness

!include services_monobusiness/payments-account-search/ext.puml
!include services_monobusiness/payments-account-search/normal.puml
!include services_monobusiness/payments-account-search/db.puml

```