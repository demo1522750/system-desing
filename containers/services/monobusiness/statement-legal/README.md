```plantuml

!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

!define services_monobusiness https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/services/monobusiness


!include services_monobusiness/statement-legal/ext.puml

!include services_monobusiness/statement-legal/normal.puml

!include services_monobusiness/statement-legal/db.puml

!include services_monobusiness/statement-legal/cache.puml


```
