```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define services_monobusiness https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/services/monobusiness

!include services_monobusiness/jb/ext.puml
!include services_monobusiness/jb/normal.puml
!include services_monobusiness/jb/queue.puml
```