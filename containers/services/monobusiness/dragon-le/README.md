```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
'!define actors https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/actors
'!define frontends https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/frontends
!define services_monobusiness https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/services/monobusiness


!include services_monobusiness/dragon-le/ext.puml
!include services_monobusiness/dragon-le/normal.puml
!include services_monobusiness/dragon-le/db.puml
!include services_monobusiness/dragon-le/queue.puml
!include services_monobusiness/dragon-le/cache.puml
```