```plantuml

!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

!define services_monobusiness https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/services/monobusiness


!include services_monobusiness/infoservice-le/ext.puml
!include services_monobusiness/infoservice-le/normal.puml
!include services_monobusiness/infoservice-le/db.puml


```
