```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
' Components
!define actors https://gitlab.com/demo1522750/system-desing/-/raw/main/containers/actors

!include actors/client.puml

!include actors/fop.puml

!include actors/director.puml

!include actors/accountant.puml

```